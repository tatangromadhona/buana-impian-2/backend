'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class GoogleDrive extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  GoogleDrive.init({
    file_id: DataTypes.STRING,
    file_name: DataTypes.STRING,
    mime_type: DataTypes.STRING,
    web_view_link: DataTypes.STRING,
    web_content_link: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'GoogleDrive',
  });
  return GoogleDrive;
};