require('dotenv').config();
const {
    ENV,
    HTTP_PORT,
    SENTRY_DSN,
} = process.env;

const Sentry = require('@sentry/node');
const Tracing = require('@sentry/tracing');
const express = require('express');
const morgan = require('morgan');
const app = express();

Sentry.init({
    dsn: SENTRY_DSN,
    environment: ENV,
    integrations: [
        new Sentry.Integrations.Http({ tracing: true }),
        new Tracing.Integrations.Express({ app }),
    ],
    tracesSampleRate: 1.0,
});

app.use(Sentry.Handlers.requestHandler());
app.use(Sentry.Handlers.tracingHandler());

app.use(express.json());
app.use(morgan('dev'));
app.set('view engine', 'ejs');

const v1 = require('./routes/v1');
app.use('/api/v1', v1);

app.use(Sentry.Handlers.errorHandler());

// handle 404 response
app.use((req, res, next) => {
    res.status(404).json({
        status: false,
        message: 'woops. looks like this endpoint doesn\'t exist.',
        data: null
    });
});

// handle error
app.use((err, req, res, next) => {
    console.error(err.stack);
    Sentry.captureException(err);
    res.status(500).json({
        status: false,
        message: err.message,
        data: null
    });
});

app.listen(HTTP_PORT, () => console.log('listening on port', HTTP_PORT));