const {
    IMAGEKIT_PUBLIC_KEY,
    IMAGEKIT_PRIVATE_KEY,
    IMAGEKIT_URL_ENDPOINT,
} = process.env;

var ImageKit = require('imagekit');

var imagekit = new ImageKit({
    publicKey: IMAGEKIT_PUBLIC_KEY,
    privateKey: IMAGEKIT_PRIVATE_KEY,
    urlEndpoint: IMAGEKIT_URL_ENDPOINT
});

module.exports = {
    upload: (fileName, file) => {
        return new Promise(async (resolve, reject) => {
            try {
                const result = await imagekit.upload({ file, fileName });

                return resolve(result);
            } catch (err) {
                return reject(err);
            }
        });
    },

    delete: (fileId) => {
        return new Promise(async (resolve, reject) => {
            try {
                const result = await imagekit.deleteFile(fileId);

                return resolve(result);
            } catch (err) {
                return reject(err);
            }
        });
    }
};