const multer = require('multer');
const upload = multer();

module.exports = {
    imageSingle: upload.single('image'),
    fileSingle: upload.single('file')
};