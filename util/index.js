const http = require('./http');
const drive = require('./google/drive');
const gmail = require('./google/gmail');
const oauth2 = require('./google/oauth2');
const multer = require('./multer');
const imagekit = require('./imagekit');

module.exports = {
    http,
    drive,
    oauth2,
    multer,
    imagekit,
    mailer: gmail
};