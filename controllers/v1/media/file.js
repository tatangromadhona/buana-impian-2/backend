const { drive } = require('../../../util');
const { Readable } = require('stream');
const { GoogleDrive } = require('../../../models');

module.exports = {
    store: async (req, res, next) => {
        const { originalname, mimetype, buffer } = req.file;
        const stream = Readable.from(buffer.toString());

        try {
            const data = await drive.upload(originalname, mimetype, stream);

            // generate public link
            const { webViewLink, webContentLink } = await drive.generatePublicLink(data.id);

            const file = await GoogleDrive.create({
                file_id: data.id,
                file_name: data.name,
                mime_type: data.mimeType,
                web_view_link: webViewLink,
                web_content_link: webContentLink
            });

            res.status(201).json({
                status: true,
                message: 'file uploaded!',
                data: file
            });

        } catch (err) {
            return next(err);
        }
    },

    index: async (req, res, next) => {
        try {
            const files = await GoogleDrive.findAll();

            return res.status(200).json({
                status: true,
                message: 'ok',
                data: files
            });
        } catch (err) {
            return next(err);
        }
    },

    show: async (req, res, next) => {
        try {
            const { id } = req.params;

            const file = await GoogleDrive.findOne({ where: { id } });
            if (!file) {
                return res.status(404).json({
                    status: false,
                    message: 'file not found!',
                    data: null
                });
            }

            return res.status(200).json({
                status: true,
                message: 'ok',
                data: file
            });
            
        } catch (err) {
            return next(err);
        }
    },

    destroy: async (req, res, next) => {
        try {
            const { id } = req.params;

            const file = await GoogleDrive.findOne({ where: { id } });

            if (!file) {
                return res.status(404).json({
                    status: false,
                    message: 'file not found!',
                    data: null
                });
            }

            // delete file in drive
            await drive.delete(file.file_id);

            const deleted = await GoogleDrive.destroy({ where: { id } });

            return res.status(200).json({
                status: false,
                message: 'ok',
                data: {
                    affected: deleted
                }
            });

        } catch (err) {
            return next(err);
        }
    }
};